export interface IProfile {
  id: number;
  uuid: string;
  name: string;
  gender: 'male' | 'female';
  username: string;
  company: string;
  avatarUrl: string;
}

export interface IUser {
  id: number;
  uuid: string;
  email: string;
  profiles: {
    profileId: number
  }[];
  lastLogin?: Date;
}
