import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { IProfile } from '../types/types';
import { custom } from '@logging';

@Injectable({
  providedIn: 'root'
})
export class ProfileApiService {

  constructor() { }

  public getProfileById(profileId: number): Observable<IProfile> {
    const profile = profiles.find(p => p.id === profileId);
    if (!profile) {
      const error = {
        status: 404,
        msg: `Profile with id ${profileId} not found.`
      };
      custom.error(`Profile API => getId: ${profileId}`, error);
      return throwError(() => error);
    }
    custom.log(`Profile API => getId: ${profileId}`, profile);
    return of(profile);
  }
}


const profiles: IProfile[] = [
  {
    id: 100,
    uuid: "e5f02c22-23d1-4a86-a7e5-770092167569",
    name: "Banks Davidson",
    gender: "male",
    username: "Catherine",
    company: "Deminimum",
    avatarUrl: "https://picsum.photos/30?random=0"
  },
  {
    id: 101,
    uuid: "62637eab-7f19-42dc-94b0-1d4d05b32ec6",
    name: "Butler Monroe",
    gender: "male",
    username: "Effie",
    company: "Rodemco",
    avatarUrl: "https://picsum.photos/30?random=1"
  },
  {
    id: 102,
    uuid: "dd0e7ce4-7d45-4d3e-8b83-cbc25507dd36",
    name: "Lou Joyner",
    gender: "female",
    username: "Evangeline",
    company: "Ewaves",
    avatarUrl: "https://picsum.photos/30?random=2"
  },
  {
    id: 103,
    uuid: "5b88759f-7d2b-44f3-9234-d4f031d5a6d0",
    name: "Brady Key",
    gender: "male",
    username: "Addie",
    company: "Comveyer",
    avatarUrl: "https://picsum.photos/30?random=3"
  },
  {
    id: 104,
    uuid: "9779cd8d-0fea-49f0-8cd9-80f4e170e8a1",
    name: "Hancock Frost",
    gender: "male",
    username: "Cooper",
    company: "Pushcart",
    avatarUrl: "https://picsum.photos/30?random=4"
  },
  {
    id: 105,
    uuid: "29d3b3fd-1d66-4357-9b24-0ccdf02f1a7f",
    name: "Wiggins Stark",
    gender: "male",
    username: "Moody",
    company: "Earwax",
    avatarUrl: "https://picsum.photos/30?random=5"
  },
  {
    id: 106,
    uuid: "bbdab843-2bb0-4566-902d-e55ff6656dc1",
    name: "Snyder Mcguire",
    gender: "male",
    username: "Livingston",
    company: "Gology",
    avatarUrl: "https://picsum.photos/30?random=6"
  },
  {
    id: 107,
    uuid: "2bc90405-1e4a-43ec-88f2-2b09c9f824d6",
    name: "Mcclure Lawrence",
    gender: "male",
    username: "Lindsey",
    company: "Exodoc",
    avatarUrl: "https://picsum.photos/30?random=7"
  },
  {
    id: 108,
    uuid: "7ad3ca56-5d80-4fb8-a8d1-6187b20bf7a2",
    name: "Sykes Hunt",
    gender: "male",
    username: "Zelma",
    company: "Filodyne",
    avatarUrl: "https://picsum.photos/30?random=8"
  },
  {
    id: 109,
    uuid: "46aa7b78-ba0b-427f-9d7a-2965c604c30e",
    name: "Dorothy Hudson",
    gender: "female",
    username: "Katheryn",
    company: "Polarium",
    avatarUrl: "https://picsum.photos/30?random=9"
  }
];
