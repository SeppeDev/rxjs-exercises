import { Injectable } from '@angular/core';
import { BehaviorSubject, defer, map, Observable, of, tap } from 'rxjs';
import { IUser } from '../types/types';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private _user$ = new BehaviorSubject<IUser>(null);

  private defaultUser: IUser = {
    id: 1234,
    uuid: '6e0eb6f3-5b9f-44c0-b3fa-bca78cfde77a',
    email: 'testuser@gmail.com',
    profiles: [
      {
        profileId: 101
      },
      {
        profileId: 102
      },
      {
        profileId: 103
      }
    ]
  }
  private currentDate$ = defer(() => of(new Date()));

  constructor() { }

  public getCurrentUser$(): Observable<IUser> {
    return this._user$.asObservable();
  }

  public getLoggedInState$(): boolean {
    // Adjust below to code to check if the user exists
    return false;
  }

  public login(): Observable<IUser> {
    return this.currentDate$
      .pipe(
        map(lastLogin => ({ ...this.defaultUser, lastLogin })),
        tap(user => this._user$.next(user))
      );
  }

  public refreshToken(): Observable<IUser> {
    return this.currentDate$
      .pipe(
        map(lastLogin => ({ ...this.defaultUser, lastLogin })),
        tap(user => this._user$.next(user))
      );
  }

  public logout(): void {
    this._user$.next(null);
  }
}
