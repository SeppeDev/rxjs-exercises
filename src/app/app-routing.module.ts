import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QueryParamComponent } from './components/query-param/query-param.component';
import { AuthComponent } from './components/auth/auth.component';

const routes: Routes = [
  {
    path: 'query-param',
    component: QueryParamComponent
  },
  {
    path: 'auth',
    component: AuthComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
