import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  constructor(
    private _snackBar: MatSnackBar,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit(): void {
  }

  login() {
    // this.authenticationService.login();
    this.showSnackbar('LOGIN success')
  }

  logout() {
    // this.authenticationService.logout();
    this.showSnackbar('LOGOUT success')
  }

  refreshToken() {
    // this.authenticationService.refreshToken();
    this.showSnackbar('Refresh done')
  }

  private showSnackbar(msg: string): void {
    this._snackBar.open(msg, 'Close')
  }

}
