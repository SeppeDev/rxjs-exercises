import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProfileApiService } from '../../services/profile-api.service';
import { IProfile } from '../../types/types';

@Component({
  selector: 'app-query-param',
  templateUrl: './query-param.component.html',
  styleUrls: ['./query-param.component.scss']
})
export class QueryParamComponent implements OnInit {
  public profile: IProfile | null;

  constructor(
    private activatedRoute: ActivatedRoute,
    private profileApiService: ProfileApiService,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.queryParams
      .subscribe(params => console.log(params));

    /* fn to get profile by id:
     this.profileApiService.getProfileById(102);
     */
  }

}
