class CustomLogging {
  public log(body: string = '', response?: object): void {
    console.log(
      `%c${body}`,
      `color: green; font-size: 0.8rem;`,
      response ? response : ''
    );
  }

  public error(body: string = '', error?: object): void {
    console.log(
      `%c${body}`,
      `color: red; font-size: 0.8rem;`,
      error ? error : ''
    );
  }
}

export const custom = new CustomLogging;
